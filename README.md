# DAWN 

Este juego se ha hecho para la competición de matamarcianos de febrero de 2020 
de [DIV Compo 2020](https://divcompo.now.sh).

[Repositorio](https://gitlab.com/AzazelN28/juego-compo-1)
[Devlog](https://gitlab.com/AzazelN28/juego-compo-1-devlog)

## Introducción

31 de enero 2050
Laboratorios Kappa
Planeta enano Ceres (Cinturón de Asteroides)

---

PROF. CAJAL: Mire subdelegado, entiendo perfectamente la situación pero como comprenderá nosotros debemos seguir unos protocolos de seguridad y...

SUB. SUNG: ¡A la mierda los protocolos! La situación en la tierra es insostenible, y como comprenderá yo necesito responder ante mis superiores. Se acercan elecciones y en el partido están todos muy interesados en que el proyecto salga adelante, necesito... necesitamos resultados ¡y los necesitamos ya!

SUB. SUNG: Así que si quiere que sigamos financiando sus proyectos, ya sabe lo que tiene que hacer.

---

PROF. CAJAL: ¡Menúdo imbécil! Cuánto más caro es el traje, más imbécil es el político... pero ya le habéis oído, necesitamos resultados, pasad la prueba 35 a la fase de producción.

PROF/A. SERVET: Pero profesor... todavía no hemos terminado con el cultivo aislado. No sabemos qué ocurrirá una vez la bactería esté en un entorno no controlado. Necesitamos pasar estas pruebas.

PROF. CAJAL: Ya me ha oído, prueba 35 en producción.

---

Pasillo se tiñe de rojo y suenan alarmas. Se cierran puertas.

Se ve una especie de masa informe cogiendo a un científico por una pierna y sosteniéndolo boca abajo.

---

Título

## Controles

| Acción          | Jugador 1   | Jugador 2    |
|-----------------|-------------|--------------|
| Mover Arriba    | Arriba      | W            |
| Mover Abajo     | Abajo       | S            |
| Mover Izquierda | Izquierda   | A            |
| Mover Derecha   | Derecha     | D            |
| Disparar        | Mayus. Der  | Q            |
| Usar            | Espacio     | E            |

Made with :heart: by [AzazelN28](https://github.com/AzazelN28)
