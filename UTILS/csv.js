function parse(data, skipFirstRow = false) {
  const rows = data.split('\n').map(row => row.split(','))
  if (skipFirstRow) {
    return rows.filter((_, index) => index > 0)
  }
  return rows
}

module.exports = {
  parse
}
