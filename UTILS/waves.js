const fs = require('fs')
const path = require('path')
const csv = require('./csv')

const WAVES = 1024
const WAVE_PROPS = 6
const WAVE_PROP_SIZE = 4

const text = fs.readFileSync(process.argv[2], 'utf-8')
const rows = csv.parse(text, true)

const buffer = Buffer.alloc(WAVES * WAVE_PROPS * WAVE_PROP_SIZE)

let offset = 0
for (const row of rows) {
  for (const col of row) {
    buffer.writeUInt32LE(parseFloat(col), offset)
    offset += 4
  }
}

fs.writeFileSync(
  path.join(
    path.dirname(process.argv[2]),
    `${path.basename(process.argv[2], path.extname(process.argv[2])).toUpperCase()}.DAT`
  ),
  buffer
)
